<?php
	date_default_timezone_set('America/Caracas');
	session_start();

	$inicio = $_POST['inicio1'];
	$final = $_POST['final'];

	$fecha_inicio = new DateTime($inicio);
	$fecha_final = new DateTime($final);

	$diferencia = $fecha_inicio->diff($fecha_final);
	// la var diferencia es un objeto, se accede a sus datos mediante el operador '->'
	/*
		Si las fecha se tuvieron que invertir para hacer el calculo el valor de invert será 1 por lo que
		la fecha de inicio es mayor que la fecha final y no debe ser asi
	*/
	if($diferencia->invert == 1){
		$_SESSION['mensaje-color'] = 'danger';
		$_SESSION['mensaje'] = "La fecha de inicio de usuarios con mayor prestámo debe ser menor que la fecha final";
		echo "<script type='text/javascript'>
			window.location='./reportes.php';
			</script>";
	}

	$_SESSION['fecha_inicio'] = $fecha_inicio;
	$_SESSION['fecha_final'] = $fecha_final;
	echo "<script type='text/javascript'>window.location='./reportes.php';</script>";
	
?>