<?php
	session_start();
	include('conexion.php');
	$conexion = conexion();

	header("Content-type: text/html; charset=utf8");
	$usuario = $_SESSION['dato_usuario'];
	
	if (!isset($_POST['nombre'])) {
		$_SESSION['mensaje'] = "No se ha recibido nombre en el POST";
		echo "<script type='text/javascript'>
				alert('No se ha recibido nombre en el POST ');
				window.location='./agregar_usuario.php';
		</script>";
	}
	$nombre = $_POST['nombre'];
	$apellido = $_POST['apellido'];
    $cedula = $_POST['cedula'];
    $telefono = $_POST['telefono'];
    $correo = $_POST['correo'];
    $carrera =$_POST['carrera'];
    $id_adm = $_POST['id_adm'];
    $estatus = "ACTIVO";

	$sql = "SELECT * FROM usuario WHERE cedula = '$cedula'";
	$resultado = mysqli_query($conexion,$sql) or die(mysqli_error()); 

	if (mysqli_num_rows($resultado) > 0) 
	{
		$_SESSION['mensaje'] = 'Cédula ya existente.';
		$_SESSION['mensaje-color'] = 'warning';
		echo "<script type='text/javascript'>
				alert('Cédula ya existente.');
				window.location='./agregar_usuario.php';
		</script>";
	}else
		{
			$sql = "SELECT * FROM usuario WHERE correo = '$correo'";
			$resultado = mysqli_query($conexion,$sql) or die(mysqli_error()); 

			if (mysqli_num_rows($resultado) > 0) 
			{
				$_SESSION['mensaje'] = 'Correo ya existente.';
				$_SESSION['mensaje-color'] = 'danger';
				echo "<script type='text/javascript'>
						alert('Correo ya existente.');
						window.location='./agregar_usuario.php';
				</script>";
			}else
			{
				$sql = "INSERT INTO usuario VALUES (null,'$nombre','$apellido','$cedula','$telefono','$correo','$carrera',NOW(),'$estatus','$id_adm')";
				$resultado = mysqli_query($conexion,$sql) or die(mysqli_error());

				$_SESSION['mensaje'] = 'Se ha agregado a "'.$nombre.' '.$apellido.'" con éxito';
				$_SESSION['mensaje-color'] = 'success';
				echo "<script type='text/javascript'>
							// alert('Usuario agregado con éxito.');
							window.location='./index_usuarios.php';
					</script>";
				mysqli_close($conexion);

			}
		}
	
?>
