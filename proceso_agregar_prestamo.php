<?php
	include('conexion.php');
	$conexion = conexion();
	
	$libro = $_POST['libro'];
	$idnuevo = explode(" -- ",$libro);
	$id_libro = $idnuevo[0];

	$usuario = $_POST['usuario'];
	$id_nuevo = explode(" -- ",$usuario);
	$id_estudiante = $id_nuevo[0];

	$estatus = "PENDIENTE";
	$estatus_dos = "COMPLETADO";
	
	$inicio = $_POST['inicio'];
	$final = $_POST['final'];
	$id_adm = $_POST['id_adm'];
	$ejemplar = $_POST['ejemplar'];

	// print_r($_POST);

	$fecha_inicio = new DateTime($inicio);
	$fecha_final = new DateTime($final);

	$dia_ini = $fecha_inicio->format('d');
	$dia_fina = $fecha_final->format('d');

	$año_ini = $fecha_inicio->format('y');
	$año_fina = $fecha_final->format('y');

	$mes_ini = $fecha_inicio->format('m');
	$mes_fina = $fecha_final->format('m');

	// verificar si el usuario está activo
	$sql = "SELECT * FROM usuario WHERE id = '$id_estudiante'";
	$resultado = mysqli_query($conexion, $sql) or die(mysqli_error());
	$row = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
	
	// print_r($row);
	// print_r($fecha_final);
	// echo "<br>";
	// print_r($fecha_inicio);
	// die();

	if ($row['estatus'] != "ACTIVO") {
		$_SESSION['mensaje'] = "El lector ".$row['nombre']." ".$row['apellido']." no se encuentra activo.";
		$_SESSION['mensaje-color'] = 'warning';
		echo "<script type='text/javascript'>
				//alert('Préstamo registrado con éxito.');
				window.location='./agregar_prestamo.php';
		</script>";
	}

	if (($año_fina-$año_ini) < 0) 
	{
		$_SESSION['mensaje'] = "Ingrese un año correcto.";
		$_SESSION['mensaje-color'] = 'danger';
		echo "<script type='text/javascript'>
				//alert('Préstamo registrado con éxito.');
				window.location='./agregar_prestamo.php';
		</script>";
	}else
	{
		if (($mes_fina-$mes_ini) < 0) 
		{
			$_SESSION['mensaje'] = "Ingrese un mes correcto.";
			$_SESSION['mensaje-color'] = 'danger';
				echo "<script type='text/javascript'>
							//alert('Préstamo registrado con éxito.');
							window.location='./agregar_prestamo.php';
					</script>";
		}else
		{
			if (($dia_fina-$dia_ini) < 0) 
			{
				$_SESSION['mensaje'] = "Ingrese un día correcto.";
				$_SESSION['mensaje-color'] = 'danger';
				echo "<script type='text/javascript'>
							//alert('Préstamo registrado con éxito.');
							window.location='./agregar_prestamo.php';
					</script>";
			}else
			{
				$sql = "SELECT * FROM libro WHERE id = '$id_libro'";
				$resultado = mysqli_query($conexion, $sql) or die(mysqli_error());
				$row = mysqli_fetch_array($resultado, MYSQLI_ASSOC);

					// echo "Jesus no está mirando";

				if ($row['ejemplar'] >= $ejemplar && $ejemplar > 0) {
					
					// echo "estoy en el ejemplar >= ejemplar and ejemplar > 0";

					$sql = "SELECT * FROM prestamo WHERE id_libro = '$id_libro' and estatus != '$estatus_dos' and ejemplar = '$ejemplar'";
					$resultado = mysqli_query($conexion, $sql) or die(mysqli_error($conexion));
					
					
					// print_r($resultado);die();

					if (mysqli_num_rows($resultado) > 0) 
					{
						$_SESSION['mensaje'] = "Volumen no disponible. Debe indicar otro volumen.";
						$_SESSION['mensaje-color'] = 'warning';
						echo "<script type='text/javascript'>
								//alert('Préstamo registrado con éxito.');
								window.location='./agregar_prestamo.php';
						</script>";
					}else{
							$sql = "SELECT * FROM libro WHERE id = '$id_libro'";
							$resultado = mysqli_query($conexion,$sql) or die(mysqli_error());
							$row = mysqli_fetch_array($resultado, MYSQLI_ASSOC);

							// print_r($row);die();

							$sq = "SELECT * FROM prestamo WHERE id_libro = '$id_libro' ";
							$datos = mysqli_query($conexion,$sq) or die(mysqli_error()); 
							
							$cantidad = $row['ejemplar'] - mysqli_num_rows($datos);
						
							if ($cantidad > 0) 
							{
								$cantidad = $cantidad - 1;

								$sql = "INSERT INTO prestamo VALUES (null,'$inicio','$final','$id_libro','$id_estudiante','$id_adm','$estatus','$ejemplar')";
								$resultado = mysqli_query($conexion,$sql) or die(mysqli_error());

								$_SESSION['mensaje'] = "Préstamo registrado con éxito.";
								$_SESSION['mensaje-color'] = 'success';
								echo "<script type='text/javascript'>
											//alert('Préstamo registrado con éxito.');
											window.location='./listar_prestamos.php';
									</script>";
							}else
							{
								$_SESSION['mensaje'] = "Libro no disponible.";
								$_SESSION['mensaje-color'] = 'warning';
								echo "<script type='text/javascript'>
											//alert('Préstamo registrado con éxito.');
											window.location='./agregar_prestamo.php';
									</script>";
							}
					}
				}else{
					$_SESSION['mensaje'] = "Por favor, coloque un volumen válido.";
					$_SESSION['mensaje-color'] = 'warning';
					echo "<script type='text/javascript'>
							//alert('Préstamo registrado con éxito.');
							window.location='./agregar_prestamo.php';
					</script>";
				}

			}
		}
	}
	mysqli_close($conexion);
?>
