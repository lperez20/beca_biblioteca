<?php
	session_start();
	if(!isset($_SESSION["dato_usuario"]))
		{ 
			header('Location: index.html'); 
		}
	
	// Estoy obteniendo los datos del usuario que almacene la variable de sesion
	if(isset($_SESSION["datos_usuario"])){
		$datos_usuario = $_SESSION['datos_usuario'];
		// unset($_SESSION['datos_usuario']);
	}
	include_once("./conexion.php");
	$conexion = conexion();
?>
<html lang="es">
<head>
	<title>Reportes</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" type="text/css" media="all" href="./assets/css/jquery-ui.min.css" />
	<link rel="stylesheet" href="./assets/bootstrap337/css/bootstrap.css" />
	<!-- <link rel="stylesheet" href="./assets/css/bootstrap-datepicker.css" /> -->
	<link rel="stylesheet" href="./assets/css/main.css" />
	<style type="text/css">
		.ui-datepicker table{ font-size: 0.7em !important; }
		.ui-datepicker .ui-datepicker-title{ font-size: 0.9em !important; }
		.form-group { margin-bottom: 0px; }
	</style>
</head>

<body>
	<?php include('./menu_usuario_global.php'); ?>
	<div id="main">
		<section id="top" class="one facyt">
		<!-- <section id="top" class="two"> -->
			<div class="container">
				<header>
					<h2>Reportes</h2>
				</header>
			</div>
		</section>
		<div class="titulo">Libros Más Consultados</div>
	
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<!-- PARA OBTENER LAS ENTRADAS DE LA BASE DE DATOS -->
					<?php if (isset($_SESSION['fecha_inicio']) && isset($_SESSION['fecha_final'])): 

						$sql = "SELECT l.titulo, l.autor, a.nombre, COUNT(p.id_libro) AS cantidad FROM prestamo AS p, libro AS l, admin AS a WHERE p.id_libro = l.id AND p.id_adm = a.id AND p.inicio >= '".$_SESSION['fecha_inicio']->format('Y-m-d')."' AND p.inicio <= '".$_SESSION['fecha_final']->format('Y-m-d')."' GROUP BY p.id_libro ORDER BY `cantidad` DESC LIMIT 10";
					?>
					<?php else: 
						$fecha_inicio = new DateTime('TODAY');
						$fecha_final = new DateTime('TODAY');

						$fecha_inicio->modify('-2 month'); //Hace dos meses

						$_SESSION['fecha_inicio'] = $fecha_inicio;
						$_SESSION['fecha_final'] = $fecha_final;

						$sql = "SELECT l.titulo, l.autor, a.nombre, COUNT(p.id_libro) AS cantidad FROM prestamo AS p, libro AS l, admin AS a WHERE p.id_libro = l.id AND p.id_adm = a.id AND p.inicio >= '".$_SESSION['fecha_inicio']->format('Y-m-d')."' AND p.inicio <= '".$_SESSION['fecha_final']->format('Y-m-d')."' GROUP BY p.id_libro ORDER BY `cantidad` DESC LIMIT 10";
					?>
					<?php endif;
						// echo $sql;
						$res = mysqli_query($conexion, $sql);
						while($resultado[] = $res->fetch_assoc());
					?>
					 <p style="margin-bottom: 15px;"></p>
					 <form action="procesar_reporte1.php" id="formulario1" method="post" class="form-horizontal">
					 	<div class="form-group">
						 	<div class="col-lg-5">
						 		<div class="row">
						 			<label class="control-label col-sm-5" for="inicio1">Fecha inicio:</label>
							 		<div class="col-sm-5">
										<input class="form-control" value="<?php echo $_SESSION['fecha_inicio']->format('Y-m-d') ?>" type="text"  maxlength="50"  name="inicio1" id="date_ini_a" required/>
									</div>
						 		</div>
						 	</div>
						 	<div class="col-lg-5">
						 		<div class="row">
						 			<label class="control-label col-sm-5" for="final">Fecha final:</label>
							 		<div class="col-sm-5">
										<input class="form-control" value="<?php echo $_SESSION['fecha_final']->format('Y-m-d') ?>" type="text"  maxlength="50"  name="final" id="date_end_a" required/>
									</div>
						 		</div>
						 	</div>
						 	<div class="col-lg-2">
					 			<button from="formulario1" class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i> Buscar</button>
						 	</div>
					 	</div>
					 </form>

					 <div class="table-responsive">
						 <table id="dataTable" class="table table-bordered table-stripped">
						 	<thead>
						 		<tr>
						 			<th>Título</th>
						 			<th>Autor</th>	
						 			<th>Cantidad de veces prestado</th>
						 			<th>Ingresado Por</th>
						 		</tr>
						 	</thead>
						 	<tbody>

						 	<?php
						 	if(sizeof($resultado) > 0){
						 	foreach ($resultado as $fila) {
						 			if ($fila != NULL) {
						 	 ?>
						 		<tr>
						 			<td><?php echo $fila['titulo']; ?></td>
						 			<td><?php echo $fila['autor']; ?></td>
						 			<td><?php echo $fila['cantidad']; ?></td>
						 			<td><?php echo $fila['nombre']; ?></td>
						 		</tr>
						 		<?php } } }?>
						 	</tbody>
						 </table>
					</div>
				</div>
			</div>
			<br>
			
			<div class="titulo row">Usuarios Con Mayor Préstamo</div>

			<div class="row">
				<div class="col-lg-12">
					<!-- PARA OBTENER LAS ENTRADAS DE LA BASE DE DATOS -->
					<?php if (isset($_SESSION['fecha_iniciob']) && isset($_SESSION['fecha_finalb'])):
						$sq = "SELECT u.nombre, u.apellido, COUNT(p.id_usuario) AS cantidad, a.nombre AS nombre_adm FROM prestamo AS p, usuario AS u, admin AS a WHERE p.id_usuario = u.id AND p.id_adm = a.id AND p.inicio >= '".$_SESSION['fecha_iniciob']->format('Y-m-d')."' AND p.inicio <= '".$_SESSION['fecha_finalb']->format('Y-m-d')."' GROUP BY p.id_usuario ORDER BY `cantidad` DESC LIMIT  10";
					?>
					<?php else:

						$fecha_inicio = new DateTime('TODAY');
						$fecha_final = new DateTime('TODAY');

						$fecha_inicio->modify('-2 month'); //Hace dos meses

						$_SESSION['fecha_iniciob'] = $fecha_inicio;
						$_SESSION['fecha_finalb'] = $fecha_final;

						$sq = "SELECT u.nombre, u.apellido, COUNT(p.id_usuario) AS cantidad, a.nombre AS nombre_adm FROM prestamo AS p, usuario AS u, admin AS a WHERE p.id_usuario = u.id AND p.id_adm = a.id AND p.inicio >= '".$_SESSION['fecha_iniciob']->format('Y-m-d')."' AND p.inicio <= '".$_SESSION['fecha_finalb']->format('Y-m-d')."' GROUP BY p.id_usuario ORDER BY `cantidad` DESC LIMIT  10";
					?>
					<?php endif ?>
					<?php

						$re = mysqli_query($conexion, $sq);
						while($res_usuarios[] = $re->fetch_assoc());
					 ?>
					 <p style="margin-bottom: 15px;"></p>
					 <form action="procesar_reporte2.php" id="formulario2" method="post" class="form-horizontal">
					 	<div class="form-group">
						 	<div class="col-lg-5">
						 		<div class="row">
						 			<label class="control-label col-sm-5" for="inicio1">Fecha inicio:</label>
							 		<div class="col-sm-5">
										<input class="form-control" type="text" value="<?php echo $_SESSION['fecha_iniciob']->format('Y-m-d') ?>" name="inicio1" id="date_ini_b" required/>
									</div>
						 		</div>
						 	</div>
						 	<div class="col-lg-5">
						 		<div class="row">
						 			<label class="control-label col-sm-5" for="final">Fecha final:</label>
							 		<div class="col-sm-5">
										<input class="form-control" type="text" value="<?php echo $_SESSION['fecha_finalb']->format('Y-m-d') ?>" name="final" id="date_end_b" required/>
									</div>
						 		</div>
						 	</div>
						 	<div class="col-lg-2">
					 			<button class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i> Buscar</button>
						 	</div>
					 	</div>
					 </form>
					 <div class="table-responsive">
						 <table id="dataTable2" class="table table-bordered table-stripped">
						 	<thead>
						 		<tr>
						 			<th>Nombre</th>
						 			<th>Apellido</th>	
						 			<th>Cantidad de prestámos</th>
						 			<th>Ingresado Por</th>
						 		</tr>
						 	</thead>
						 	<tbody>
						 	<?php foreach ($res_usuarios as $fil) {
						 			if ($fil != NULL) {
						 	 ?>
						 		<tr>
						 			<td><?php echo $fil['nombre']; ?></td>
						 			<td><?php echo $fil['apellido']; ?></td>
						 			<td><?php echo $fil['cantidad']; ?></td>
						 			<td><?php echo $fil['nombre_adm']; ?></td>
						 		</tr>
						 		<?php } } ?>
						 	</tbody>
						 </table>
					</div>
				</div>
			</div>
			<br>
			<br>
		</div>
		<?php include_once('./footer.php'); ?>
	</div>
</body>
</html>
<!-- Inicializando la dataTable -->
<script type="text/javascript" src="./assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./assets/js/espanol.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#date_ini_a").datepicker({ dateFormat: 'yy-mm-dd' });
		$("#date_end_a").datepicker({ dateFormat: 'yy-mm-dd' });

		$("#date_ini_b").datepicker({ dateFormat: 'yy-mm-dd' });
		$("#date_end_b").datepicker({ dateFormat: 'yy-mm-dd' });

		$('#dataTable').DataTable({
			// 'stateSave': true,
			"bLengthChange": false,
			'bFilter': false,
            "language": {
                "url": "./assets/dataTables/lang/spanish.json"
            },
            'info' : true,
            "order": [[ 2, "desc" ]]
            // columnDefs: [{ orderable: false, targets: [2] }]
        });

        $('#dataTable2').DataTable({
			// 'stateSave': true,
			'bFilter': false,
			"bLengthChange": false,
            "language": {
                "url": "./assets/dataTables/lang/spanish.json"
            },
            'info' : true,
            "order": [[ 2, "desc" ]]
            // columnDefs: [{ orderable: false, targets: [2] }]
        });

        $('a[id="eliminar_libro"]').on('click', function(){
			var $href = $( this ).data('href');
			libro_nombre = $( this ).data('nombre')

			jQuery.confirm({
				theme: 'supervan',
				columnClass: 'medium',
				title: '¡Atención!',
				content: '¿Desea eliminar el libro "'+libro_nombre+'"?',
				buttons: {
			        "Eliminar Libro": function(){
			            jQuery.alert({theme: 'supervan', title: false, content:'Eliminando el libro ...'});
			            location.href = $href;
			        },
			        Cancelar: function(){
			        	jQuery.alert({theme: 'supervan', title: false, content: 'No se efectuaran cambios sobre el libro ...'});
			        }
			    }
			});
		});
	});
</script>