/* La sentencia de abajo es la sentencia final. Se va a ejecutar todos los dias a las 11:55 pm. */

CREATE EVENT verificar_prestamos
    ON SCHEDULE EVERY 1 DAY 
	STARTS '2016-01-01 23:55:00'
    DO
      UPDATE prestamo SET estatus = "VENCIDO" WHERE final < DATE_FORMAT(NOW(), "%d-%m-%Y") AND estatus != "COMPLETADO";

/* La sentencia de abajo es solo para que realices pruebas rapido xD El codigo de arriba es el final */

CREATE EVENT verificar_prestamos
    ON SCHEDULE EVERY 2 MINUTE
        STARTS CURRENT_TIMESTAMP
    DO
      UPDATE prestamo SET estatus = "VENCIDO" WHERE final < DATE_FORMAT(NOW(), "%d-%m-%Y") AND estatus != "COMPLETADO";

/* Sentencia para borrar el evento */

DROP EVENT verificar_prestamos;

/*Consulta el evento */

SHOW PROCESSLIST

/*Activo el evento*/

SET GLOBAL event_scheduler = ON;

